const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    name:{ type: String },
    email: { type: String },
    mblno: { type: String },
    gender: String,
    bloodgroup: {type : String},
    designation:{ type: String },
    image: {type: String  },
});

module.exports = mongoose.model('Employee',employeeSchema);
