const express = require("express");
const multer = require("multer");
const Employee = require("../models/employee");

const router = express.Router();


router.put('/:id', (req,res,next) =>{

Employee.findById(req.params.id, function(err, doc) {
      if (err)
          res.status(500).json({ status: 'error', message: 'Database Error:' + err, docs: '' });

          doc.name = req.body.name;
          doc.email = req.body.email;
          doc.mblno = req.body.mblno;
          doc.gender = req.body.gender;
          doc.designation = req.body.designation;
          doc.bloodgroup = req.body.bloodgroup;



      doc.save(function(err) {
          if (err) {
              res.status(500).json({ status: 'error', message: 'Database Error:' + err, docs: '' });
          } else {

              res.status(200).json({ status: 'success', message: 'Document updated Successfully', docs: '' });
          }
      });
  });


});


router.get('',(req,res,next)=>{
  let hg = 0;
  let bloodGroup;
  Employee.aggregate([
                    {"$group" : {_id:"$bloodgroup", count:{$sum:1}}}
                    ]).then((data) =>{
    data.forEach(ele=>{
      if(ele.count>hg){
        hg = ele.count;
        bloodGroup = ele._id;
      }
      // console.log(ele._id);
      // console.log(ele.count);
    })
    res.json({bloodGroup:bloodGroup});



  });

});
module.exports = router;
