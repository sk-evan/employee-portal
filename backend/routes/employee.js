const express = require("express");
const multer = require("multer");
const Employee = require("../models/employee");

const router = express.Router();
const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg' : 'jpg',
  'image/jpg': 'jpg'
};

const storage = multer.diskStorage({

  destination: (req,file,cb) =>{
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if(isValid){
      error = null;
    }
    cb(error, "backend/images");
  },
  filename:(rq,file,cb) => {

    const name = file.originalname
    .toLowerCase().split(' ').join('-');
    const  ext = MIME_TYPE_MAP[file.mimetype];
    cb(null,name + '-' + Date.now() + '.' +
     ext );
  }
});


router.post('',multer({storage: storage}).single("image"),(req,res,next) =>{
const url = req.protocol + '://' + req.get("host");

const employee = new Employee();
employee.name= req.body.name;
employee.email = req.body.email;
employee.mblno = req.body.mblno;
employee.gender = req.body.gender;
employee.designation = req.body.designation;
employee.bloodgroup = req.body.bloodgroup;
  if(req.file != null){
    employee.image =  url + "/images/" + req.file.filename;
  }


employee.save().then(createPost=>{
     res.status(201).json({
    // message:"Post Added Successfully.",
    // employee : {
    //   ...createPost,
    //   id: createPost._id
    // }
  });
  });
});

router.get('',(req,res,next)=>{
  Employee.find().then((documents) =>{

  //   res.status(200).json({
  //   message:"Data inserted Successfully1",
  //   employee: documents ,
  // });


   res.send(documents);


  });

});



router.delete('/:id',  (req,res,next) =>{
 Employee.deleteOne({_id: req.params.id}).then((result) =>{
  if(result.n > 0 ){
    res.status(200).json({message: 'Post Deleted!'});
    }else{
      res.status(401).json({ message: 'Not Authorized.. '});

    }


 });

});




 router.put('/:id', multer({ storage: storage }).single("image"), (req, res) => {

  Employee.findById(req.params.id, function(err, doc) {
      if (err)
          res.status(500).json({ status: 'error', message: 'Database Error:' + err, docs: '' });


      const url = req.protocol + "://" + req.get('host');

          if (req.file != null) {
              doc.image = url + "/images/" + req.file.filename;
          } else {
              doc.image = null;
          }


      doc.save(function(err) {
          if (err) {
              res.status(500).json({ status: 'error', message: 'Database Error:' + err, docs: '' });
          } else {

              res.status(200).json({ status: 'success', message: 'Document updated Successfully', docs: '' });
          }
      });
  });
})


module.exports = router;
