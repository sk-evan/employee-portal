const app = require("./backend/app");

const http = require("http");


const port = 3300;

app.set("port", port);

const server = http.createServer(app);

server.listen(port);
console.log("Running...");
