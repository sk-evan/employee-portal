import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/shared/employee.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

   form: FormGroup;
   flag: boolean;



  @Input() emp: any;


  constructor(private employeeService: EmployeeService , private modelRef: BsModalRef) { }

  ngOnInit() {

    console.log(this.emp);

    this.flag = false;



    this.form = new FormGroup({
      name: new FormControl('', {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      email: new FormControl('', {
        validators: [Validators.required]
      }),
      mblno: new FormControl('', {
        validators: [Validators.required]
      }),
      gender: new FormControl('', {
        validators: [Validators.required]
      }),
      designation: new FormControl('', {
        validators: [Validators.required]
      }),
      bloodgroup: new FormControl('', {
        validators: [Validators.required]
      }),

  });
  }


  onEdit(Data) {

      console.log(Data.value);
      this.employeeService.updateEmployee(Data.value , this.emp._id);
      this.employeeService.refresh();
      this.modelRef.hide();
      this.edit(this.flag);

  }


  edit(flag) {

    this.flag = !flag;
  }

  onDeleteEmployee() {
    this.employeeService.DeleteEmployee(this.emp._id)
    .subscribe((res) => {
      this.modelRef.hide();
      this.employeeService.refresh();
    });
  }
}
