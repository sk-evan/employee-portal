import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Employee } from 'src/app/shared/employee.model';

@Component({
  selector: 'app-popup-modal',
  templateUrl: './popup-modal.component.html',
  styleUrls: ['./popup-modal.component.css']
})
export class PopupModalComponent implements OnInit {

  email;
  name;
  mblno;
  designation;
  bloodgroup;
  _id;
  gender;
  image: any;
  emp: any ;
  data: any;
  img;




  // public form: FormData;

  constructor(
    private modalService: BsModalService,
    private modalRef: BsModalRef
  ) {}

  ngOnInit() {



    if (this.image === null){

    this.image = 'assets/sk.png';

    }
    this.data = {
      img : this.image,
      _id: this._id,
    };


    this.emp = {
      _id: this._id,
      name: this.name,
      email: this.email,
      mblno: this.mblno,
      designation: this.designation,
      bloodgroup: this.bloodgroup,
      gender: this.gender,
    };

  }
}
