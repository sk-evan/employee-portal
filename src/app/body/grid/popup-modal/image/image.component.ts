import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/shared/employee.service';
declare var jQuery: any;

@Component({
  selector: 'app-image',

  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  constructor(private employeeService: EmployeeService) { }

  @Input() employeeData: any;

  form: FormGroup;


imagePreview: string;
public file;




  ngOnInit() {



    this.form = new FormGroup({
    image: new FormControl({
      validators: [Validators.required],
    })
});


}

onImagePicked(event: Event) {
   this.file = (event.target as HTMLInputElement).files[0];
   const reader = new FileReader();
   this.UpdateImage(this.file);
   reader.onload = () => {
    this.employeeData.img = reader.result as string;
  };
   reader.readAsDataURL(this.file);

}


UpdateImage(Data) {

  const imgData = new FormData();
  if (Data != null) {
    imgData.append('image', Data);
  }
  this.employeeService.editProfileImage(imgData , this.employeeData._id);

}


removePhoto(event: Event) {

      this.employeeData.img = 'assets/sk.png';
      this.UpdateImage(null);
      this.file = null;
        }

}
