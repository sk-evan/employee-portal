import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/shared/employee.service';
declare var jQuery: any;
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  imagePreview: string;
  form: FormGroup;

  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.imagePreview = 'assets/sk.png';
    this.form = new FormGroup({
      name: new FormControl('', {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      email: new FormControl('', {
        validators: [Validators.required]
      }),
      mblno: new FormControl('', {
        validators: [Validators.required]
      }),
      gender: new FormControl('', {
        validators: [Validators.required]
      }),
      designation: new FormControl('', {
        validators: [Validators.required]
      }),
      bloodgroup: new FormControl('', {
        validators: [Validators.required]
      }),
      image: new FormControl({
        validators: [Validators.required]
      })
    });

  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      image: file
    });
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSavePost(Data) {


    if(this.form.invalid){
      return true;
    }
    this.employeeService.addEmployee(Data.value).subscribe(res => {
      this.employeeService.refresh();
      this.form.reset();
      // tslint:disable-next-line: deprecation
      this.removePhoto(event);
    });

    // jQuery('#exampleModal').modal('hide');
  }

  removePhoto(event: Event) {
    this.imagePreview = 'assets/sk.png';
    this.form.patchValue({
      image: 'assets/sk.png'
    });
  }
}
