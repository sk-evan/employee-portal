import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeeService } from 'src/app/shared/employee.service';
import { Employee } from 'src/app/shared/employee.model';
import { Subscription } from 'rxjs';
import { PopupModalComponent } from './popup-modal/popup-modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DragulaService } from 'ng2-dragula';
import * as dragula from 'dragula';
declare var jQuery: any;

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],

})
export class GridComponent implements OnInit  {

  public employees: Employee[] = [];
  imagePreview: string;
  userId: string;
  userFilter: any = { title: '' };
  selected: string;
   modalRef: BsModalRef;
   msg = '';

  // modalService: any;

  constructor( private employeeService: EmployeeService, private modalService: BsModalService,
               private dragulaService: DragulaService) {

      this.dragulaService.createGroup('vampires', {
        revertOnSpill: true,
      });
      // this.dragulaService.createGroup('bag-task2', {
      //   removeOnSpill: true,
      // });
      // this.dragulaService.createGroup('bag-task3', {
      //   copy: true,
      // });

     }

  ngOnInit() {
     this.imagePreview = 'assets/sk.png';

  }




  employeeInfo(employee) {
    const initialState = employee;


    this.modalRef = this.modalService.show(PopupModalComponent, {initialState}, );
    this.modalRef.setClass('modal-lg');

    this.modalRef.content.closeBtnName = 'Close';

  }



  refresh($event) {

    this.employees = $event;

  }

}
