import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from 'src/app/shared/employee.service';
import { Employee } from 'src/app/shared/employee.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  employees: [];

  @Output() messageEvent = new EventEmitter<any>();


  constructor( private employeeService: EmployeeService) { }

  ngOnInit() {

    this.getEmployeeDetails();
    this.employeeService.reload$.subscribe(() => {
      this.getEmployeeDetails();

      this.employeeService.getBloodGroup()
    .subscribe((res: any) => {
      this.employeeService.setbloodgroup = res.bloodGroup;
    });
  });
  }


  getEmployeeDetails() {
    this.employeeService.getEmployee().subscribe( responseData => {




      this.employees = responseData;

      this.employeeService.getEmployeeNo(this.employees.length);

      this.messageEvent.emit(this.employees);
    });
  }



}
