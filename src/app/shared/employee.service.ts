import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Employee } from './employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient, private router: Router) {}


  private employeePosts: Employee[] = [];
  setbloodgroup: any;
  public noOfEmployees = 0;

    private updateData = new Subject<any>();
    reload$ = this.updateData.asObservable();

  addEmployee(employee: Employee) {
    const employeeData = new FormData();
    employeeData.append('name', employee.name);
    employeeData.append('email', employee.email);
    employeeData.append('mblno', employee.mblno);
    employeeData.append('gender', employee.gender);
    employeeData.append('designation', employee.designation);
    employeeData.append('bloodgroup', employee.bloodgroup);
    employeeData.append('image', employee.image);

    return  this.http.post<any>(
        'http://localhost:3300/employee',
        employeeData
      );
      // .subscribe(responseData => {
      //   const employee: Employee = {
      //     id: responseData.employee.id,
      //     name: responseData.employee.name,
      //     email: responseData.employee.email,
      //     mblno: responseData.employee.mblno,
      //     gender: responseData.employee.gender,
      //     bloodgroup: responseData.employee.bloodgroup,
      //     designation: responseData.employee.designation,
      //     image: responseData.employee.image
      //   };
      //   this.employeePosts.push(employee);
      //   this.employeePostUpdated.next([...this.employeePosts]);
      // });
  }

  // getPostUpdateListener() {
  //   return this.employeePostUpdated.asObservable();
  // }

  getEmployee() {
    // tslint:disable-next-line: no-unused-expression
    return this.http.get< any>('http://localhost:3300/employee');
    // .pipe(
    //   map(postData => {
    //     return postData.employee.map(emp => {
    //       return {
    //         id: emp._id,
    //         name: emp.name,
    //         email: emp.email,
    //         mblno: emp.mblno,
    //         gender: emp.gender,
    //         designation: emp.designation,
    //         bloodgroup: emp.bloodgroup,
    //         image: emp.image
    //       };
    //     });
    //   })
    // )
    // .subscribe(transformPost => {
    //   this.employeePosts = transformPost;
    //   this.employeePostUpdated.next([...this.employeePosts]);
    // });
}

  getEmployeeNo(count) {
    this.noOfEmployees = count;
  }




  updateEmployee(employee, id) {

    this.http.put<any>('http://localhost:3300/edit/' + id, employee)
    .subscribe(res =>{
      this.updateData.next();
    });

    }

    // tslint:disable-next-line: adjacent-overload-signatures
    DeleteEmployee(empId) {
         return this.http.delete('http://localhost:3300/employee/' + empId);


    }

    getBloodGroup() {
      return this.http.get<any>('http://localhost:3300/edit');

    }

        refresh() {
          this.updateData.next();
        }



        editProfileImage(data , id){

         this.http.put<any>('http://localhost:3300/employee/'  + id , data)

          .subscribe(res =>{

            this.updateData.next();

          });


        }


}
