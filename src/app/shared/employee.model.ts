export interface Employee {

  id: string ;
  name: string;
  email: string;
  mblno: string;
  gender: string;
  designation: string;
  bloodgroup: string;
  image: string;

}
