import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { GridComponent } from './body/grid/grid.component';
import { ProfileComponent } from './body/grid/profile/profile.component';
import { AddEmployeeComponent } from './body/grid/add-employee/add-employee.component';
import { PopupModalComponent } from './body/grid/popup-modal/popup-modal.component';
import { ImageComponent } from './body/grid/popup-modal/image/image.component';
import { UserDetailsComponent } from './body/grid/popup-modal/user-details/user-details.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DndModule } from 'ngx-drag-drop';
import { NgDragDropModule } from 'ng-drag-drop';
import { DragulaModule } from 'ng2-dragula';






@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    GridComponent,
    ProfileComponent,
    AddEmployeeComponent,
    PopupModalComponent,
    ImageComponent,
    UserDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DndModule,
    NgDragDropModule.forRoot(),
    ModalModule.forRoot(),
    DragulaModule.forRoot()

  ],
  entryComponents : [
    PopupModalComponent
 ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
