import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared/employee.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  bloodgroup: any;


  constructor(private employeeService: EmployeeService) { }

  public maximummBloodGroup;
  ngOnInit(){
    this.setBloodGroup();
    this.employeeService.reload$.subscribe(() => {
        this.setBloodGroup();
    })
  }

  setBloodGroup(){
    this.employeeService.getBloodGroup().subscribe((res) => {
      this.maximummBloodGroup = res.bloodGroup;
    })
  }

}
